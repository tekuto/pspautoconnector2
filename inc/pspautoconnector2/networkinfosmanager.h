﻿#ifndef PSPAUTOCONNECTOR2_NETWORKINFOSMANAGER_H
#define PSPAUTOCONNECTOR2_NETWORKINFOSMANAGER_H

#include "pspautoconnector2/networkinfos.h"
#include "pspautoconnector2/iwlib.h"

#include <mutex>
#include <condition_variable>
#include <string>

namespace pspautoconnector2 {
    struct NetworkInfosManager
    {
        std::mutex              mutex;
        std::condition_variable condition;

        bool            updated = false;
        NetworkInfos    networkInfos;
    };

    bool update(
        NetworkInfosManager &
        , const int &
        , const std::string &
        , const decltype( iw_range::we_version_compiled ) &
        , NetworkInfos &
    );

    void swapWhenUpdated(
        NetworkInfos &
        , NetworkInfosManager &
    );

    void waitUpdate(
        NetworkInfosManager &
    );
}

#endif  // PSPAUTOCONNECTOR2_NETWORKINFOSMANAGER_H
