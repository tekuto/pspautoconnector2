﻿#ifndef PSPAUTOCONNECTOR2_NETWORKINFOS_H
#define PSPAUTOCONNECTOR2_NETWORKINFOS_H

#include "pspautoconnector2/networkinfo.h"

#include <vector>
#include <string>
#include <iwlib.h>

namespace pspautoconnector2 {
    typedef std::vector< NetworkInfo > NetworkInfos;

    bool getNetworkInfos(
        NetworkInfos &
        , const int &
        , const std::string &
        , const decltype( iw_range::we_version_compiled ) &
    );
}

#endif  // PSPAUTOCONNECTOR2_NETWORKINFOS_H
