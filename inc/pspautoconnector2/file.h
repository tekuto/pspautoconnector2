﻿#ifndef PSPAUTOCONNECTOR2_FILE_H
#define PSPAUTOCONNECTOR2_FILE_H

#include <string>

namespace pspautoconnector2 {
    bool readFile(
        std::string &
        , const char *
    );
}

#endif  // PSPAUTOCONNECTOR2_FILE_H
