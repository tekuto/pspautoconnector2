﻿#ifndef PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_MAP_H
#define PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_MAP_H

#include "pspautoconnector2/def/config/element/string.h"
#include "pspautoconnector2/def/config/element/element.h"

#include <map>
#include <memory>

namespace pspautoconnector2 {
    typedef std::map<
        ConfigElementString
        , ConfigElementUnique
    > ConfigElementMap;

    typedef std::unique_ptr< ConfigElementMap > ConfigElementMapUnique;
}

#endif  // PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_MAP_H
