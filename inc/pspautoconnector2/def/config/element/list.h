﻿#ifndef PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_LIST_H
#define PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_LIST_H

#include "pspautoconnector2/def/config/element/element.h"

#include <vector>
#include <memory>

namespace pspautoconnector2 {
    typedef std::vector< ConfigElementUnique > ConfigElementList;

    typedef std::unique_ptr< ConfigElementList > ConfigElementListUnique;
}

#endif  // PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_LIST_H
