﻿#ifndef PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_STRING_H
#define PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_STRING_H

#include <string>
#include <memory>

namespace pspautoconnector2 {
    typedef std::string ConfigElementString;

    typedef std::unique_ptr< ConfigElementString > ConfigElementStringUnique;
}

#endif  // PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_STRING_H
