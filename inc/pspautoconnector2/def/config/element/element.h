﻿#ifndef PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_ELEMENT_H
#define PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_ELEMENT_H

#include <memory>

namespace pspautoconnector2 {
    struct ConfigElement;

    typedef std::unique_ptr< ConfigElement > ConfigElementUnique;
}

#endif  // PSPAUTOCONNECTOR2_DEF_CONFIG_ELEMENT_ELEMENT_H
