﻿#ifndef PSPAUTOCONNECTOR2_NETWORKSTATUS_H
#define PSPAUTOCONNECTOR2_NETWORKSTATUS_H

namespace pspautoconnector2 {
    enum NetworkStatus
    {
        NOT_CONNECTED,
        CHANGED,
        CHECKING,
        CONNECTED,
    };
}

#endif  // PSPAUTOCONNECTOR2_NETWORKSTATUS_H
