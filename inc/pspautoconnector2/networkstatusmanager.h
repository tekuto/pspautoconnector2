﻿#ifndef PSPAUTOCONNECTOR2_NETWORKSTATUSMANAGER_H
#define PSPAUTOCONNECTOR2_NETWORKSTATUSMANAGER_H

#include "pspautoconnector2/networkstatus.h"

#include <mutex>
#include <condition_variable>

namespace pspautoconnector2 {
    struct NetworkStatusManager
    {
        std::mutex              mutex;
        std::condition_variable condition;

        NetworkStatus   status = NetworkStatus::NOT_CONNECTED;
    };

    NetworkStatus getNetworkStatus(
        NetworkStatusManager &
    );

    void waitWhenNotConnected(
        NetworkStatusManager &
    );

    void waitNotConnected(
        NetworkStatusManager &
    );

    void waitWhenChanged(
        NetworkStatusManager &
    );

    void waitWhenChecking(
        NetworkStatusManager &
    );

    void changed(
        NetworkStatusManager &
    );

    void checkingWhenChanged(
        NetworkStatusManager &
    );

    bool missingWhenNotChanged(
        NetworkStatusManager &
    );

    void connected(
        NetworkStatusManager &
    );
}

#endif  // PSPAUTOCONNECTOR2_NETWORKSTATUSMANAGER_H
