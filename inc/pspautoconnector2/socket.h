﻿#ifndef PSPAUTOCONNECTOR2_SOCKET_H
#define PSPAUTOCONNECTOR2_SOCKET_H

#include "pspautoconnector2/time.h"

#include <array>
#include <memory>
#include <string>

namespace pspautoconnector2 {
    const auto  MAC_ADDRESS_LENGTH = 6;

    typedef std::array<
        unsigned char
        , MAC_ADDRESS_LENGTH
    > MacAddress;

    struct CloseSocket
    {
        void operator()(
            int *
        ) const;
    };

    typedef std::unique_ptr<
        int
        , CloseSocket
    > SocketCloser;

    bool initSocket(
        int &
    );

    bool bind(
        const int &
        , const std::string &
    );

    bool isExistsPsp(
        const int &
        , const MacAddress &
        , const Time &
    );

    bool initMacAddress(
        MacAddress &
        , const std::string &
    );
}

#endif  // PSPAUTOCONNECTOR2_SOCKET_H
