﻿#ifndef PSPAUTOCONNECTOR2_CONFIG_FILE_H
#define PSPAUTOCONNECTOR2_CONFIG_FILE_H

#include "pspautoconnector2/def/config/element/element.h"
#include "pspautoconnector2/time.h"
#include "pspautoconnector2/socket.h"

#include <string>
#include <set>

namespace pspautoconnector2 {
    struct ConfigFile
    {
        struct Searcher
        {
            std::string interface;
            Time        intervalUseconds;
        };

        struct Changer
        {
            std::string             interface;
            Time                    timeoutFindNetworkUseconds;
            Time                    intervalFindNetworkUseconds;
            size_t                  historySize;
            Time                    reconnectableUseconds;
            bool                    enableForceChange;
            size_t                  ssidPrefixSize;
            std::set< std::string > forceChangeTargets;
        };

        struct Connector
        {
            std::string interface;
            MacAddress  pspMacAddress;
            Time        timeoutCheckConnectUseconds;
        };

        Searcher    searcher;
        Changer     changer;
        Connector   connector;
    };

    bool initConfigFile(
        ConfigFile &
        , const ConfigElement &
    );
}

#endif  // PSPAUTOCONNECTOR2_CONFIG_FILE_H
