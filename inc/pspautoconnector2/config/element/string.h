﻿#ifndef PSPAUTOCONNECTOR2_CONFIG_ELEMENT_STRING_H
#define PSPAUTOCONNECTOR2_CONFIG_ELEMENT_STRING_H

#include "pspautoconnector2/def/config/element/string.h"

namespace pspautoconnector2 {
    const char * init(
        ConfigElementStringUnique &
        , const char *
        , const char *
    );
}

#endif  // PSPAUTOCONNECTOR2_CONFIG_ELEMENT_STRING_H
