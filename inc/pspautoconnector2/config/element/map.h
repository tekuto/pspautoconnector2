﻿#ifndef PSPAUTOCONNECTOR2_CONFIG_ELEMENT_MAP_H
#define PSPAUTOCONNECTOR2_CONFIG_ELEMENT_MAP_H

#include "pspautoconnector2/def/config/element/map.h"
#include "pspautoconnector2/def/config/element/string.h"

namespace pspautoconnector2 {
    const char * init(
        ConfigElementMapUnique &
        , const char *
        , const char *
    );

    const ConfigElementString * getString(
        const ConfigElementMap &
        , const char *
    );
}

#endif  // PSPAUTOCONNECTOR2_CONFIG_ELEMENT_MAP_H
