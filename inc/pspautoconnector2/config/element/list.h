﻿#ifndef PSPAUTOCONNECTOR2_CONFIG_ELEMENT_LIST_H
#define PSPAUTOCONNECTOR2_CONFIG_ELEMENT_LIST_H

#include "pspautoconnector2/def/config/element/list.h"

namespace pspautoconnector2 {
    const char * init(
        ConfigElementListUnique &
        , const char *
        , const char *
    );
}

#endif  // PSPAUTOCONNECTOR2_CONFIG_ELEMENT_LIST_H
