﻿#ifndef PSPAUTOCONNECTOR2_CONFIG_ELEMENT_ELEMENT_H
#define PSPAUTOCONNECTOR2_CONFIG_ELEMENT_ELEMENT_H

#include "pspautoconnector2/def/config/element/element.h"
#include "pspautoconnector2/def/config/element/string.h"
#include "pspautoconnector2/def/config/element/list.h"
#include "pspautoconnector2/def/config/element/map.h"

#include <string>

namespace pspautoconnector2 {
    struct ConfigElement
    {
        ConfigElementStringUnique   stringUnique;
        ConfigElementListUnique     listUnique;
        ConfigElementMapUnique      mapUnique;
    };

    ConfigElement * newConfigElement(
        const std::string &
    );

    const char * getElement(
        const char *
        , const char *
    );

    const char * init(
        ConfigElementUnique &
        , const char *
        , const char *
    );

    const ConfigElementString * getString(
        const ConfigElement &
    );

    const ConfigElementList * getList(
        const ConfigElement &
    );

    const ConfigElementMap * getMap(
        const ConfigElement &
    );
}

#endif  // PSPAUTOCONNECTOR2_CONFIG_ELEMENT_ELEMENT_H
