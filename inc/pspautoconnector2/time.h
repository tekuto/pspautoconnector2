﻿#ifndef PSPAUTOCONNECTOR2_TIME_H
#define PSPAUTOCONNECTOR2_TIME_H

#include <string>
#include <functional>

namespace pspautoconnector2 {
    typedef long long Time;

    bool strToTime(
        Time &
        , const std::string &
    );

    bool intervalProc(
        const Time &
        , const std::function< bool( bool & ) > &
    );

    bool timeoutProc(
        const Time &
        , const Time &
        , const std::function< bool( bool & ) > &
    );
}

#endif  // PSPAUTOCONNECTOR2_TIME_H
