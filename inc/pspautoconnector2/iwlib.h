﻿#ifndef PSPAUTOCONNECTOR2_IWLIB_H
#define PSPAUTOCONNECTOR2_IWLIB_H

#include <iwlib.h>
#include <memory>
#include <string>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace pspautoconnector2 {
    struct CloseIwSocket
    {
        void operator()(
            int *
        ) const;
    };

    typedef std::unique_ptr<
        int
        , CloseIwSocket
    > IwSocketCloser;

    bool initIwSocket(
        int &
    );

    template< typename FUNCTION_T >
    bool iwSetExt(
        const int &             _IW_SOCKET
        , const std::string &   _INTERFACE
        , int                   _request
        , const FUNCTION_T &    _FUNCTION
    )
    {
        auto    iwReq = iwreq();
        std::memset(
            &iwReq
            , 0
            , sizeof( iwReq )
        );

        _FUNCTION( iwReq );

        if( iw_set_ext(
            _IW_SOCKET
            , _INTERFACE.c_str()
            , _request
            , &iwReq
        ) < 0 ) {
#ifdef  DEBUG
            std::printf( "E:iw_set_ext()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool getWeVersion(
        decltype( iw_range::we_version_compiled ) &
        , const int &
        , const std::string &
    );
}

#endif  // PSPAUTOCONNECTOR2_IWLIB_H
