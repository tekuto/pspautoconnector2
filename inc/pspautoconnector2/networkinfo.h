﻿#ifndef PSPAUTOCONNECTOR2_NETWORKINFO_H
#define PSPAUTOCONNECTOR2_NETWORKINFO_H

#include <string>
#include <iwlib.h>

namespace pspautoconnector2 {
    struct NetworkInfo
    {
        std::string ssid;
        iw_freq     freq;
    };

    bool equals(
        const NetworkInfo &
        , const NetworkInfo &
    );

    bool equals(
        const NetworkInfo &
        , const std::string &
        , const iw_freq &
    );

    bool connect(
        const NetworkInfo &
        , const int &
        , const std::string &
    );
}

#endif  // PSPAUTOCONNECTOR2_NETWORKINFO_H
