﻿#ifndef PSPAUTOCONNECTOR2_NETWORKINFOHISTORY_H
#define PSPAUTOCONNECTOR2_NETWORKINFOHISTORY_H

#include "pspautoconnector2/networkinfos.h"
#include "pspautoconnector2/networkinfo.h"
#include "pspautoconnector2/time.h"

#include <chrono>
#include <vector>

namespace pspautoconnector2 {
    struct NetworkInfoHistory
    {
        struct HistoryElement
        {
            NetworkInfo                             networkInfo;
            std::chrono::steady_clock::time_point   updateTime;
        };

        typedef std::vector< HistoryElement > History;

        size_t  maxSize;
        History history;
    };

    const NetworkInfo * getNextNetworkInfo(
        const NetworkInfoHistory &
        , const NetworkInfos &
        , const Time &
    );

    const NetworkInfo * getForceChangeNetworkInfo(
        const NetworkInfoHistory &
        , const NetworkInfos &
        , const NetworkInfo &
        , const size_t &
        , const Time &
    );

    bool add(
        NetworkInfoHistory &
        , const NetworkInfo &
    );
}

#endif  // PSPAUTOCONNECTOR2_NETWORKINFOHISTORY_H
