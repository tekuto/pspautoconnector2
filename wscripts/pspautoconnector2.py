# -*- coding: utf-8 -*-

from .builder import cpp

def getDependModules(
):
    return [
    ]

def build(
    _context,
):
    cpp.program(
        _context,
        'pspautoconnector2',
        [
            'main.cpp',
            'networkinfo.cpp',
            'networkinfos.cpp',
            'networkinfosmanager.cpp',
            'networkinfohistory.cpp',
            'networkstatusmanager.cpp',
            'iwlib.cpp',
            'socket.cpp',
            'time.cpp',
            'file.cpp',
            {
                'config' : [
                    'file.cpp',
                    {
                        'element' : [
                            'element.cpp',
                            'string.cpp',
                            'list.cpp',
                            'map.cpp',
                        ],
                    },
                ],
            },
        ],
        lib = [
            'iw',
        ],
    )
