# -*- coding: utf-8 -*-

from .common import generateSources
from .. import common

import os
import os.path
import shutil
import filecmp

def cp(
    _context,
    _target,
    _sources,
):
    _context(
        rule = _cp,
        target = _generateTarget( _target ),
        source = _generateSources(
            _sources,
            _target,
        ),
    )

def _cp(
    _task,
):
    TARGET = _task.outputs[ 0 ].abspath()

    if os.path.exists( TARGET ) == False:
        os.makedirs( TARGET )

    for input in _task.inputs:
        SOURCE = input.abspath()

        TARGET_FILE = os.path.join(
            TARGET,
            str( input ),
        )

        if _compareFile(
            TARGET_FILE,
            SOURCE,
        ) == True:
            continue

        shutil.copy(
            SOURCE,
            TARGET,
        )

    return None

def _compareFile(
    _target,
    _source,
):
    if os.path.exists( _target ) == False:
        return False

    if filecmp.cmp(
        _source,
        _target,
    ) == False:
        return False

    return True

def _generateTarget(
    _target,
):
    return os.path.join(
        common.TEST_DIR,
        _target,
    )

def _generateSources(
    _sources,
    _target,
):
    return generateSources(
        _sources,
        os.path.join(
            common.TESTDATA_DIR,
            _target,
        ),
    )
