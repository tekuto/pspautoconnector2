# -*- coding: utf-8 -*-

PSPAUTOCONNECTOR2 = 'pspautoconnector2'

APPNAME = PSPAUTOCONNECTOR2

BUILD_DIR = 'build'
TEST_DIR = 'test'

SOURCE_DIR = 'src'
INCLUDE_DIR = 'inc'
TESTDATA_DIR = 'testdata'
