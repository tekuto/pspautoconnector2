# -*- coding: utf-8 -*-

from wscripts import common
from wscripts import version
from wscripts import cmdoption

from pydoc import ModuleScanner
import os.path
import importlib

APPNAME = common.APPNAME
VERSION = version.getString()

out = common.BUILD_DIR

def _generateFlags(
    common = [],
    debug = [],
    release = [],
):
    return {
        BUILD : common + OPTIONS
        for BUILD, OPTIONS in {
            cmdoption.BUILD_DEBUG : debug,
            cmdoption.BUILD_RELEASE : release,
        }.items()
    }

_CXXFLAGS_BASES = {
    cmdoption.COMPILER_TYPE_GCC : _generateFlags(
        common = [
            '-Wall',
            '-fno-rtti',
            '-fvisibility=hidden',
            '-std=c++14',
        ],
        debug = [
            '-O0',
            '-g',
        ],
        release = [
            '-O2',
        ],
    ),
    cmdoption.COMPILER_TYPE_MSVC : _generateFlags(
        common = [
            '/Wall',
            '/GR-',
            '/nologo',
            '/EHs',
        ],
        debug = [
            '/MDd',
            '/Od',
        ],
        release = [
            '/MD',
            '/O2',
            '/Oi',
            '/GL',
        ],
    ),
}

_LINKFLAGS_BASES = {
    cmdoption.LINKER_TYPE_LD : _generateFlags(
        common = [
            '-pthread',
        ],
    ),
    cmdoption.LINKER_TYPE_MSVC : _generateFlags(
        common = [
            '/NOLOGO',
            '/DYNAMICBASE',
            '/NXCOMPAT',
        ],
        release = [
            '/OPT:REF',
            '/OPT:ICF',
            '/LTCG',
        ],
    ),
}

_DEFINES = {
    cmdoption.COMPILER_TYPE_GCC : _generateFlags(
        common = [
            'COMPILER_TYPE_GCC',
        ],
        debug = [
            'DEBUG',
        ],
    ),
    cmdoption.COMPILER_TYPE_MSVC : _generateFlags(
        common = [
            'COMPILER_TYPE_MSVC',
        ],
        debug = [
            'DEBUG',
        ],
    ),
}

_MODULE_OPTION_PREFIX = 'enable.'

def options(
    _context,
):
    for KEY, DEFAULT in cmdoption.OPTIONS.items():
        _context.add_option(
            _optionKey( KEY ),
            action = 'store',
            default = DEFAULT,
        )

    for module in _getModuleNames( APPNAME ):
        _context.add_option(
            _optionModuleKey( module ),
            action = 'store_true',
            default = False,
        )

    _context.load( 'compiler_cxx' )
    _context.load( 'waf_unit_test' )

def _getModuleNames(
    _PACKAGE,
):
    WSCRIPTS_PATH = 'wscripts.'
    PACKAGE_PATH = WSCRIPTS_PATH + _PACKAGE

    modules = []
    ModuleScanner().run(
        lambda *args : modules.append( args[ 1 ] ),
        key = PACKAGE_PATH,
    )

    LENGTH = len( WSCRIPTS_PATH )

    return [ m[ LENGTH: ] for m in modules ]

def _optionModuleKey(
    _MODULE,
):
    return _optionKey( _MODULE_OPTION_PREFIX + _MODULE )

def _optionKey(
    _KEY,
):
    return '--' + _KEY

def configure(
    _context,
):
    _configureBuildModules( _context )
    _configureBuild( _context )
    _configureCompiler( _context )
    _configureLinker( _context )
    _configurePath( _context )
    _configureLib( _context )

    _context.load( 'compiler_cxx' )
    _context.load( 'waf_unit_test' )

def _configureBuildModules(
    _context,
):
    LENGTH = len( _MODULE_OPTION_PREFIX )

    buildModules = []
    for key, value in vars( _context.options ).items():
        if key[ :LENGTH ] != _MODULE_OPTION_PREFIX:
            continue
        if value == False:
            continue

        MODULE = key[ LENGTH: ]

        _addBuildModules(
            buildModules,
            MODULE,
        )

    _context.msg(
        'build modules',
        buildModules,
    )

    _context.env.MY_BUILD_MODULES = buildModules

def _addBuildModules(
    _buildModules,
    _MODULE,
):
    if _MODULE in _buildModules:
        return

    _buildModules.append( _MODULE )

    module = _importModule( _MODULE )

    DEPEND_MODULES = module.getDependModules()

    for DEPEND_MODULE in DEPEND_MODULES:
        _addBuildModules(
            _buildModules,
            DEPEND_MODULE,
        )

def _configureBuild(
    _context,
):
    BUILD = _context.options.build
    _context.msg(
        cmdoption.BUILD,
        BUILD,
    )

    if BUILD == cmdoption.BUILD_DEBUG:
        return
    elif BUILD == cmdoption.BUILD_RELEASE:
        return

    _context.fatal( '非対応のビルドタイプ' )

def _configureCompiler(
    _context,
):
    COMPILER_TYPE = _context.options.compilertype
    _context.msg(
        cmdoption.COMPILER_TYPE,
        COMPILER_TYPE,
    )

    _configureCxxflags(
        _context,
        COMPILER_TYPE,
    )
    _configureDefines(
        _context,
        COMPILER_TYPE,
    )

def _configureCxxflags(
    _context,
    _COMPILER_TYPE,
):
    CXXFLAGS = _configureFlags(
        _context,
        _COMPILER_TYPE,
        _CXXFLAGS_BASES,
    )
    _context.msg(
        'cxxflags',
        CXXFLAGS,
    )

    _context.env.CXXFLAGS = CXXFLAGS

def _configureDefines(
    _context,
    _COMPILER_TYPE,
):
    defines = None
    if _COMPILER_TYPE in _DEFINES:
        defines = _DEFINES[ _COMPILER_TYPE ][ _context.options.build ]

    _context.msg(
        'defines',
        defines,
    )

    _context.env.DEFINES = defines

def _configureLinker(
    _context,
):
    LINKER_TYPE = _context.options.linkertype
    _context.msg(
        cmdoption.LINKER_TYPE,
        LINKER_TYPE,
    )

    _configureLinkflags(
        _context,
        LINKER_TYPE,
    )

def _configureLinkflags(
    _context,
    _LINKER_TYPE,
):
    LINKFLAGS = _configureFlags(
        _context,
        _LINKER_TYPE,
        _LINKFLAGS_BASES,
    )
    _context.msg(
        'linkflags',
        LINKFLAGS,
    )

    _context.env.LINKFLAGS = LINKFLAGS

def _configureFlags(
    _context,
    _FLAGS_BASE,
    _FLAGS_BASES,
):
    if _FLAGS_BASE in _FLAGS_BASES:
        return _FLAGS_BASES[ _FLAGS_BASE ][ _context.options.build ]

    return None

def _configurePath(
    _context,
):
    _configureIncludes( _context )
    _configureStlibpath( _context )

def _configureIncludes(
    _context,
):
    INCLUDES = [
        os.path.abspath( i )
        for i in [
            common.INCLUDE_DIR,
        ]
    ]
    _context.msg(
        'includes',
        INCLUDES,
    )

    _context.env.INCLUDES = INCLUDES

def _configureStlibpath(
    _context,
):
    pass

def _configureLib(
    _context,
):
    pass

def build(
    _context,
):
    for module in _context.env.MY_BUILD_MODULES:
        module = _importModule( module )

        module.build(
            _context,
        )

    from waflib.Tools import waf_unit_test
    _context.add_post_fun( waf_unit_test.summary )

def _importModule(
    _MODULE,
):
    return importlib.import_module( 'wscripts.' + _MODULE )
