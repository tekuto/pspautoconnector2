﻿#include "pspautoconnector2/file.h"

#include <memory>
#include <cstdio>
#include <new>
#include <utility>
#include <vector>
#include <array>
#include <cstring>
#include <string>

namespace {
    enum {
        BUFFER_SIZE = 1024,
    };

    struct File
    {
        struct FileCloser
        {
            void operator()(
                std::FILE * _file
            ) const
            {
                std::fclose( _file );
            }
        };

        typedef std::unique_ptr<
            std::FILE
            , FileCloser
        > FileUnique;

        FileUnique  fileUnique;
    };

    typedef std::unique_ptr< File > FileUnique;

    File * newFile(
        const char *    _PATH
    )
    {
        auto    fileUnique = File::FileUnique(
            std::fopen(
                _PATH
                , "r"
            )
        );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ファイルのオープンに失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = FileUnique(
            new( std::nothrow )File{
                std::move( fileUnique ),
            }
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:ファイルの生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    bool isEof(
        File &  _this
    )
    {
        if( std::feof( _this.fileUnique.get() ) == 0 ) {
            return false;
        }

        return true;
    }

    bool read(
        File &      _this
        , char *    _buffer
        , size_t    _BUFFER_SIZE
        , size_t &  _readSize
    )
    {
        auto    filePtr = _this.fileUnique.get();

        auto    buffer = std::vector< char >( _BUFFER_SIZE );
        auto    bufferPtr = buffer.data();

        const auto  READ_SIZE = std::fread(
            bufferPtr
            , 1
            , _BUFFER_SIZE
            , filePtr
        );
        if( std::ferror( filePtr ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:ファイル読み込みエラー\n" );
#endif  // DEBUG

            return false;
        }

        std::memcpy(
            _buffer
            , bufferPtr
            , READ_SIZE
        );
        _readSize = READ_SIZE;

        return true;
    }
}

namespace pspautoconnector2 {
    bool readFile(
        std::string &   _content
        , const char *  _PATH
    )
    {
        auto    fileUnique = FileUnique( newFile( _PATH ) );
        if( fileUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ファイル\"%s\"のオープンに失敗\n"
                , _PATH
            );
#endif  // DEBUG

            return false;
        }
        auto &  file = *fileUnique;

        auto    content = std::string();

        while( isEof( file ) == false ) {
            auto    buffer = std::array< char, BUFFER_SIZE >();

            auto    bufferPtr = buffer.data();

            auto    readSize = size_t();

            if( read(
                file
                , bufferPtr
                , BUFFER_SIZE
                , readSize
            ) == false ) {
#ifdef  DEBUG
                std::printf(
                    "E:ファイル\"%s\"の読み込み中にエラーが発生\n"
                    , _PATH
                );
#endif  // DEBUG

                return false;
            }

            if( readSize > 0 ) {
                content.append(
                    bufferPtr
                    , readSize
                );
            }
        }

        _content = std::move( content );

        return true;
    }
}
