﻿#include "pspautoconnector2/networkinfos.h"
#include "pspautoconnector2/networkinfo.h"
#include "pspautoconnector2/time.h"
#include "pspautoconnector2/iwlib.h"

#include <array>
#include <string>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  SCAN_TIMEOUT_USECONDS = pspautoconnector2::Time( 10000000 );
    const auto  SCAN_INTERVAL_USECONDS = pspautoconnector2::Time( 500000 );

    const auto  PSP_SSID_HEADER = std::string( "PSP_" );

    typedef unsigned short ScanDataSize;

    typedef std::array<
        char
        , static_cast< ScanDataSize >( ~0 )
    > ScanDataBuffer;

    bool initScanNetwork(
        const int &             _IW_SOCKET
        , const std::string &   _INTERFACE
    )
    {
        return pspautoconnector2::iwSetExt(
            _IW_SOCKET
            , _INTERFACE
            , SIOCSIWSCAN
            , [](
                iwreq & _iwReq
            )
            {
                auto &  data = _iwReq.u.data;

                data.pointer = nullptr;
                data.flags = 0;
                data.length = 0;
            }
        );
    }

    bool getScanData(
        ScanDataBuffer &        _scanDataBuffer
        , ScanDataSize &        _scanDataSize
        , const int &           _IW_SOCKET
        , const std::string &   _INTERFACE
    )
    {
        auto    iwReq = iwreq();
        std::memset(
            &iwReq
            , 0
            , sizeof( iwReq )
        );
        iwReq.u.data.pointer = _scanDataBuffer.data();
        iwReq.u.data.flags = 0;
        iwReq.u.data.length = _scanDataBuffer.size();

        if( pspautoconnector2::timeoutProc(
            SCAN_TIMEOUT_USECONDS
            , SCAN_INTERVAL_USECONDS
            , [
                &_IW_SOCKET
                , &_INTERFACE
                , &iwReq
            ]
            (
                bool &  _loopEnd
            )
            {
                errno = 0;
                if( iw_get_ext(
                    _IW_SOCKET
                    , _INTERFACE.c_str()
                    , SIOCGIWSCAN
                    , &iwReq
                ) == 0 ) {
                    _loopEnd = true;
                    return true;
                }
                if( errno != EAGAIN ) {
#ifdef  DEBUG
                    std::printf( "E:iw_get_ext()に失敗\n" );
#endif  // DEBUG

                    _loopEnd = true;
                    return false;
                }

                return true;
            }
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索結果取得がタイムアウト\n" );
#endif  // DEBUG

            return false;
        }

        _scanDataSize = iwReq.u.data.length;

        return true;
    }

    void setIwMode(
        __u32 &                         _mode
        , bool &                        _exists
        , const iw_event &              _IW_EVENT
    )
    {
        _mode = _IW_EVENT.u.mode;

        _exists = true;
    }

    void setIwSsid(
        std::string &       _ssid
        , bool &            _exists
        , const iw_event &  _IW_EVENT
    )
    {
        const auto &    IW_ESSID = _IW_EVENT.u.essid;

        _ssid.assign(
            static_cast< const char * >( IW_ESSID.pointer )
            , IW_ESSID.length
        );

        _exists = true;
    }

    void setIwFreq(
        iw_freq &           _freq
        , bool &            _exists
        , const iw_event &  _IW_EVENT
    )
    {
        std::memcpy(
            &( _freq )
            , &( _IW_EVENT.u.freq )
            , sizeof( _freq )
        );

        _exists = true;
    }

    bool isValidMode(
        const __u32 &   _MODE
    )
    {
        if( _MODE != IW_MODE_ADHOC ) {
#ifdef  DEBUG
            std::printf( "E:アドホックネットワークではない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool isValidSsid(
        const std::string & _SSID
    )
    {
        const auto  SSID_LENGTH = _SSID.size();

        if( SSID_LENGTH > IW_ESSID_MAX_SIZE ) {
#ifdef  DEBUG
            std::printf( "E:SSIDが長すぎる\n" );
#endif  // DEBUG

            return false;
        }

        if( SSID_LENGTH <= 0 ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの長さが0\n" );
#endif  // DEBUG

            return false;
        }

        if( std::strncmp(
            _SSID.c_str()
            , PSP_SSID_HEADER.c_str()
            , PSP_SSID_HEADER.size()
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:PSPのSSIDではない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool isValidNetwork(
        const __u32 &           _MODE
        , const std::string &   _SSID
        , const iw_freq &       _FREQ
    )
    {
        if( isValidMode( _MODE ) == false ) {
#ifdef  DEBUG
            std::printf( "E:有効なネットワークモードではない\n" );
#endif  // DEBUG

            return false;
        }

        if( isValidSsid( _SSID ) == false ) {
#ifdef  DEBUG
            std::printf( "E:有効なSSIDではない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool isExistsNetwork(
        pspautoconnector2::NetworkInfos &   _networkInfos
        , const std::string &               _SSID
        , const iw_freq &                   _FREQ
    )
    {
        for( const auto & NETWORK_INFO : _networkInfos ) {
            if( pspautoconnector2::equals(
                NETWORK_INFO
                , _SSID
                , _FREQ
            ) == true ) {
                return true;
            }
        }

        return false;
    }
}

namespace pspautoconnector2 {
    bool getNetworkInfos(
        NetworkInfos &                                      _networkInfos
        , const int &                                       _IW_SOCKET
        , const std::string &                               _INTERFACE
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
    )
    {
        if( initScanNetwork(
            _IW_SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スキャン開始に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    scanDataBuffer = ScanDataBuffer();
        auto    scanDataSize = ScanDataSize();
        if( getScanData(
            scanDataBuffer
            , scanDataSize
            , _IW_SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スキャンデータの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    stream = stream_descr();
        iw_init_event_stream(
            &stream
            , scanDataBuffer.data()
            , scanDataSize
        );

        _networkInfos.clear();

        auto    existsMode = false;
        auto    mode = __u32();

        auto    existsSsid = false;
        auto    ssid = std::string();

        auto    existsFreq = false;
        auto    freq = iw_freq();

        auto    iwEvent = iw_event();
        while( iw_extract_event_stream(
            &stream
            , &iwEvent
            , _WE_VERSION
        ) > 0 ) {
            switch( iwEvent.cmd ) {
            case SIOCGIWMODE:
                setIwMode(
                    mode
                    , existsMode
                    , iwEvent
                );
                break;

            case SIOCGIWESSID:
                setIwSsid(
                    ssid
                    , existsSsid
                    , iwEvent
                );
                break;

            case SIOCGIWFREQ:
                setIwFreq(
                    freq
                    , existsFreq
                    , iwEvent
                );
                break;

            default:
                break;
            }

            if( existsMode == false ) {
                continue;
            } else if( existsSsid == false ) {
                continue;
            } else if( existsFreq == false ) {
                continue;
            }

            existsMode = false;
            existsSsid = false;
            existsFreq = false;

            if( isValidNetwork(
                mode
                , ssid
                , freq
            ) == false ) {
#ifdef  DEBUG
                std::printf( "D:有効なネットワークではない\n" );
#endif  // DEBUG

                continue;
            }

            if( isExistsNetwork(
                _networkInfos
                , ssid
                , freq
            ) == true ) {
#ifdef  DEBUG
                std::printf( "D:同じネットワークが既に追加済み\n" );
#endif  // DEBUG

                continue;
            }

            _networkInfos.push_back(
                NetworkInfo{
                    ssid,
                    freq,
                }
            );
        }

        return true;
    }
}
