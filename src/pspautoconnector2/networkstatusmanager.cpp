﻿#include "pspautoconnector2/networkstatusmanager.h"
#include "pspautoconnector2/networkstatus.h"

#include <mutex>

namespace pspautoconnector2 {
    NetworkStatus getNetworkStatus(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        return _this.status;
    }

    void waitWhenNotConnected(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( _this.status == NetworkStatus::NOT_CONNECTED ) {
            _this.condition.wait( lock );
        }
    }

    void waitNotConnected(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( _this.status != NetworkStatus::NOT_CONNECTED ) {
            _this.condition.wait( lock );
        }
    }

    void waitWhenChanged(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( _this.status == NetworkStatus::CHANGED ) {
            _this.condition.wait( lock );
        }
    }

    void waitWhenChecking(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( _this.status == NetworkStatus::CHECKING ) {
            _this.condition.wait( lock );
        }
    }

    void changed(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        _this.status = NetworkStatus::CHANGED;

        _this.condition.notify_all();
    }

    void checkingWhenChanged(
        NetworkStatusManager &  _this
    )
    {
        auto &  status = _this.status;

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( status != NetworkStatus::CHANGED ) {
            return;
        }

        status = NetworkStatus::CHECKING;

        return;
    }

    bool missingWhenNotChanged(
        NetworkStatusManager &  _this
    )
    {
        auto &  status = _this.status;

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( status == NetworkStatus::CHANGED ) {
            return false;
        }

        status = NetworkStatus::NOT_CONNECTED;

        _this.condition.notify_all();

        return true;
    }

    void connected(
        NetworkStatusManager &  _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        _this.status = NetworkStatus::CONNECTED;

        _this.condition.notify_all();
    }
}
