﻿#include "pspautoconnector2/networkinfo.h"
#include "pspautoconnector2/iwlib.h"

#include <string>
#include <cstring>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool changeSsid(
        const decltype( pspautoconnector2::NetworkInfo::ssid ) &    _SSID
        , const int &                                               _IW_SOCKET
        , const std::string &                                       _INTERFACE
    )
    {
        return pspautoconnector2::iwSetExt(
            _IW_SOCKET
            , _INTERFACE
            , SIOCSIWESSID
            , [
                &_SSID
            ](
                iwreq & _iwReq
            )
            {
                auto &  essid = _iwReq.u.essid;

                essid.flags = 1;
                essid.pointer = const_cast< char * >( _SSID.c_str() );
                essid.length = _SSID.size();
                if( iw_get_kernel_we_version() < 21 ) {
                    essid.length++;
                }
            }
        );
    }

    bool changeFreq(
        const decltype( pspautoconnector2::NetworkInfo::freq ) &    _FREQ
        , const int &                                               _IW_SOCKET
        , const std::string &                                       _INTERFACE
    )
    {
        return pspautoconnector2::iwSetExt(
            _IW_SOCKET
            , _INTERFACE
            , SIOCSIWFREQ
            , [
                &_FREQ
            ](
                iwreq & _iwReq
            )
            {
                auto &  freq = _iwReq.u.freq;

                std::memcpy(
                    &freq
                    , &_FREQ
                    , sizeof( freq )
                );
                freq.flags = IW_FREQ_FIXED;
            }
        );
    }
}

namespace pspautoconnector2 {
    bool equals(
        const NetworkInfo &     _NETWORK_INFO1
        , const NetworkInfo &   _NETWORK_INFO2
    )
    {
        return equals(
            _NETWORK_INFO1
            , _NETWORK_INFO2.ssid
            , _NETWORK_INFO2.freq
        );
    }

    bool equals(
        const NetworkInfo &     _THIS
        , const std::string &   _SSID
        , const iw_freq &       _FREQ
    )
    {
        if( _THIS.ssid != _SSID ) {
            return false;
        }

        const auto &    FREQ = _THIS.freq;
        if( std::memcmp(
            &FREQ
            , &_FREQ
            , sizeof( FREQ )
        ) != 0 ) {
            return false;
        }

        return true;
    }

    bool connect(
        const NetworkInfo &     _THIS
        , const int &           _IW_SOCKET
        , const std::string &   _INTERFACE
    )
    {
        if( changeSsid(
            _THIS.ssid
            , _IW_SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの変更に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( changeFreq(
            _THIS.freq
            , _IW_SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルの変更に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}
