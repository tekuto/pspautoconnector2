﻿#include "pspautoconnector2/networkinfohistory.h"
#include "pspautoconnector2/networkinfo.h"

#include <chrono>
#include <algorithm>
#include <utility>
#include <cstring>

namespace pspautoconnector2 {
    const NetworkInfo * getNextNetworkInfo(
        const NetworkInfoHistory &  _THIS
        , const NetworkInfos &      _NETWORK_INFOS
        , const Time &              _RECONNECTABLE_USECONDS
    )
    {
        auto    CURRENT_TIME = std::chrono::steady_clock::now();

        auto    historyElementPtr = static_cast< const NetworkInfoHistory::HistoryElement * >( nullptr );

        auto    networkInfoPtr = static_cast< const NetworkInfo * >( nullptr );

        for( const auto & NETWORK_INFO : _NETWORK_INFOS ) {
            auto    existsHistory = false;

            for( const auto & HISTORY_ELEMENT : _THIS.history ) {
                if( equals(
                    NETWORK_INFO
                    , HISTORY_ELEMENT.networkInfo
                ) == false ) {
                    continue;
                }

                existsHistory = true;

                const auto  DIFF_USECONDS = std::chrono::duration_cast< std::chrono::microseconds >( CURRENT_TIME - HISTORY_ELEMENT.updateTime );
                if( DIFF_USECONDS.count() < _RECONNECTABLE_USECONDS ) {
                    break;
                }

                if( historyElementPtr != nullptr && historyElementPtr < &HISTORY_ELEMENT ) {
                    break;
                }

                historyElementPtr = &HISTORY_ELEMENT;

                networkInfoPtr = &NETWORK_INFO;

                break;
            }

            if( existsHistory == false ) {
                networkInfoPtr = &NETWORK_INFO;

                break;
            }
        }

        return networkInfoPtr;
    }

    const NetworkInfo * getForceChangeNetworkInfo(
        const NetworkInfoHistory &  _THIS
        , const NetworkInfos &      _NETWORK_INFOS
        , const NetworkInfo &       _NETWORK_INFO
        , const size_t &            _SSID_PREFIX_SIZE
        , const Time &              _RECONNECTABLE_USECONDS
    )
    {
        auto    CURRENT_TIME = std::chrono::steady_clock::now();

        const auto  SSID1 = _NETWORK_INFO.ssid;

        auto    historyElementPtr = static_cast< const NetworkInfoHistory::HistoryElement * >( nullptr );

        auto    networkInfoPtr = static_cast< const NetworkInfo * >( nullptr );

        for( const auto & NETWORK_INFO : _NETWORK_INFOS ) {
            const auto  SSID2 = NETWORK_INFO.ssid;

            if( std::strncmp(
                SSID1.c_str()
                , SSID2.c_str()
                , _SSID_PREFIX_SIZE
            ) != 0 ) {
                continue;
            }

            if( SSID1 == SSID2 ) {
                continue;
            }

            auto    existsHistory = false;

            for( const auto & HISTORY_ELEMENT : _THIS.history ) {
                if( equals(
                    NETWORK_INFO
                    , HISTORY_ELEMENT.networkInfo
                ) == false ) {
                    continue;
                }

                existsHistory = true;

                const auto  DIFF_USECONDS = std::chrono::duration_cast< std::chrono::microseconds >( CURRENT_TIME - HISTORY_ELEMENT.updateTime );
                if( DIFF_USECONDS.count() < _RECONNECTABLE_USECONDS ) {
                    break;
                }

                if( historyElementPtr != nullptr && historyElementPtr < &HISTORY_ELEMENT ) {
                    break;
                }

                historyElementPtr = &HISTORY_ELEMENT;

                networkInfoPtr = &NETWORK_INFO;

                break;
            }

            if( existsHistory == false ) {
                networkInfoPtr = &NETWORK_INFO;

                break;
            }
        }

        return networkInfoPtr;
    }

    bool add(
        NetworkInfoHistory &    _this
        , const NetworkInfo &   _NETWORK_INFO
    )
    {
        auto &      history = _this.history;
        const auto  HISTORY_SIZE = history.size();

        auto    currentTime = std::chrono::steady_clock::now();

        if( HISTORY_SIZE > 0 ) {
            history[ HISTORY_SIZE - 1 ].updateTime = currentTime;
        }

        const auto  END = history.end();
        const auto  IT = std::find_if(
            history.begin()
            , END
            , [
                &_NETWORK_INFO
            ]
            (
                const NetworkInfoHistory::HistoryElement &  _HISTORY_ELEMENT
            )
            {
                return equals(
                    _HISTORY_ELEMENT.networkInfo
                    , _NETWORK_INFO
                );
            }
        );
        if( IT != END ) {
            history.erase( IT );
        }

        history.push_back(
            NetworkInfoHistory::HistoryElement{
                _NETWORK_INFO,
                currentTime,
            }
        );

        if( history.size() > _this.maxSize ) {
            history.erase( history.begin() );
        }

        return true;
    }
}
