﻿#include "pspautoconnector2/iwlib.h"

#include <iwlib.h>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace pspautoconnector2 {
    void CloseIwSocket::operator()(
        int *   _iwSocketPtr
    ) const
    {
        iw_sockets_close( *_iwSocketPtr );
    }

    bool initIwSocket(
        int &   _iwSocket
    )
    {
        auto    iwSocket = iw_sockets_open();
        if( iwSocket < 0 ) {
#ifdef  DEBUG
            std::printf( "E:iwソケット生成に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _iwSocket = std::move( iwSocket );

        return true;
    }

    bool getWeVersion(
        decltype( iw_range::we_version_compiled ) & _weVersion
        , const int &                               _IW_SOCKET
        , const std::string &                       _INTERFACE
    )
    {
        auto    iwRange = iw_range();
        if( iw_get_range_info(
            _IW_SOCKET
            , _INTERFACE.c_str()
            , &iwRange
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:レンジ情報取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _weVersion = iwRange.we_version_compiled;

        return true;
    }
}
