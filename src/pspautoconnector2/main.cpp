﻿#include "pspautoconnector2/networkinfosmanager.h"
#include "pspautoconnector2/networkinfohistory.h"
#include "pspautoconnector2/networkinfo.h"
#include "pspautoconnector2/networkinfos.h"
#include "pspautoconnector2/networkstatusmanager.h"
#include "pspautoconnector2/iwlib.h"
#include "pspautoconnector2/socket.h"
#include "pspautoconnector2/time.h"
#include "pspautoconnector2/file.h"
#include "pspautoconnector2/config/element/element.h"
#include "pspautoconnector2/config/file.h"

#include <string>
#include <thread>
#include <functional>
#include <system_error>
#include <algorithm>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool initConfigFile(
        pspautoconnector2::ConfigFile & _configFile
        , int                           _argc
        , char **                       _argv
    )
    {
        if( _argc < 2 ) {
#ifdef  DEBUG
            std::printf( "E:コマンドライン引数で設定ファイルを指定していない\n" );
#endif  // DEBUG

            return false;
        }

        const auto  CONFIG_FILE_PATH = _argv[ 1 ];

        auto    content = std::string();
        if( pspautoconnector2::readFile(
            content
            , CONFIG_FILE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイル\"%s\"の読み込みに失敗\n"
                , CONFIG_FILE_PATH
            );
#endif  // DEBUG

            return false;
        }

        auto    configElementUnique = pspautoconnector2::ConfigElementUnique(
            pspautoconnector2::newConfigElement(
                content
            )
        );
        if( configElementUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイル\"%s\"の解析に失敗\n"
                , CONFIG_FILE_PATH
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG_ELEMENT = *configElementUnique;

        auto    configFile = pspautoconnector2::ConfigFile();
        if( pspautoconnector2::initConfigFile(
            configFile
            , CONFIG_ELEMENT
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイル\"%s\"の初期化に失敗\n"
                , CONFIG_FILE_PATH
            );
#endif  // DEBUG

            return false;
        }

        _configFile = std::move( configFile );

        return true;
    }

    bool initThread(
        std::thread &                       _thread
        , const std::function< void() > &   _PROC
    )
    {
        try {
            _thread = std::thread( _PROC );
        } catch( std::system_error & ) {
#ifdef  DEBUG
            std::printf( "E:スレッドの起動に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initIwSocket(
        int &                                           _iwSocket
        , decltype( iw_range::we_version_compiled ) &   _weVersion
        , const std::string &                           _INTERFACE
    )
    {
        auto    iwSocket = int( 0 );
        if( pspautoconnector2::initIwSocket( iwSocket ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ソケット初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto    iwSocketCloser = pspautoconnector2::IwSocketCloser( &iwSocket );

        auto    weVersion = decltype( iw_range::we_version_compiled )();
        if( pspautoconnector2::getWeVersion(
            weVersion
            , iwSocket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:wireless extentionsのバージョン取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        iwSocketCloser.release();

        _iwSocket = std::move( iwSocket );
        _weVersion = std::move( weVersion );

        return true;
    }

    void threadProcUpdateNetworkInfos(
        const pspautoconnector2::ConfigFile &       _CONFIG_FILE
        , pspautoconnector2::NetworkInfosManager &  _networkInfosManager
    )
    {
        const auto &    SEARCHER = _CONFIG_FILE.searcher;
        const auto &    INTERFACE = SEARCHER.interface;
        const auto &    INTERVAL_USECONDS = SEARCHER.intervalUseconds;

        auto    iwSocket = int( 0 );
        auto    weVersion = decltype( iw_range::we_version_compiled )();
        if( initIwSocket(
            iwSocket
            , weVersion
            , INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索用ソケット初期化に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto    iwSocketCloser = pspautoconnector2::IwSocketCloser( &iwSocket );

        auto    buffer = pspautoconnector2::NetworkInfos();
        pspautoconnector2::intervalProc(
            INTERVAL_USECONDS
            , [
                &INTERFACE
                , &_networkInfosManager
                , &iwSocket
                , &weVersion
                , &buffer
            ]
            (
                bool &
            )
            {
                if( pspautoconnector2::update(
                    _networkInfosManager
                    , iwSocket
                    , INTERFACE
                    , weVersion
                    , buffer
                ) == false ) {
#ifdef  DEBUG
                    std::printf( "E:ネットワーク情報の更新に失敗\n" );
#endif  // DEBUG

                    return false;
                }

                return true;
            }
        );
    }

    const pspautoconnector2::NetworkInfo * getNextNetworkInfo(
        pspautoconnector2::NetworkInfos &               _networkInfos
        , pspautoconnector2::NetworkInfosManager &      _networkInfosManager
        , const pspautoconnector2::NetworkInfoHistory & _NETWORK_INFO_HISTORY
        , const pspautoconnector2::Time &               _RECONNECTABLE_USECONDS
    )
    {
        pspautoconnector2::swapWhenUpdated(
            _networkInfos
            , _networkInfosManager
        );
        if( _networkInfos.size() <= 0 ) {
#ifdef  DEBUG
            std::printf( "D:ネットワーク情報が存在しない\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  NETWORK_INFO_PTR = pspautoconnector2::getNextNetworkInfo(
            _NETWORK_INFO_HISTORY
            , _networkInfos
            , _RECONNECTABLE_USECONDS
        );
        if( NETWORK_INFO_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:次に接続するネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return NETWORK_INFO_PTR;
    }

    bool findNetworkInfo(
        const int &                                         _IW_SOCKET
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , const std::string &                               _INTERFACE
        , const pspautoconnector2::NetworkInfo &            _NETWORK_INFO
    )
    {
        auto    networkInfos = pspautoconnector2::NetworkInfos();
        if( pspautoconnector2::getNetworkInfos(
            networkInfos
            , _IW_SOCKET
            , _INTERFACE
            , _WE_VERSION
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        const auto  END = networkInfos.end();
        const auto  IT = std::find_if(
            networkInfos.begin()
            , END
            , [
                &_NETWORK_INFO
            ]
            (
                const pspautoconnector2::NetworkInfo &  _NETWORK_INFO2
            )
            {
                return pspautoconnector2::equals(
                    _NETWORK_INFO
                    , _NETWORK_INFO2
                );
            }
        );
        if( IT == END ) {
#ifdef  DEBUG
            std::printf( "D:ネットワークが見つからない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool networkChange(
        pspautoconnector2::NetworkStatusManager &           _networkStatusManager
        , const int &                                       _IW_SOCKET
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , const std::string &                               _INTERFACE
        , pspautoconnector2::NetworkInfoHistory &           _networkInfoHistory
        , const pspautoconnector2::NetworkInfo &            _NETWORK_INFO
        , const pspautoconnector2::Time &                   _TIMEOUT_FIND_NETWORK_USECONDS
        , const pspautoconnector2::Time &                   _INTERVAL_FIND_NETWORK_USECONDS
    )
    {
        if( pspautoconnector2::add(
            _networkInfoHistory
            , _NETWORK_INFO
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク情報履歴の追加に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( pspautoconnector2::timeoutProc(
            _TIMEOUT_FIND_NETWORK_USECONDS
            , _INTERVAL_FIND_NETWORK_USECONDS
            , [
                &_IW_SOCKET
                , &_WE_VERSION
                , &_INTERFACE
                , &_NETWORK_INFO
            ]
            (
                bool &  _loopEnd
            )
            {
                if( findNetworkInfo(
                    _IW_SOCKET
                    , _WE_VERSION
                    , _INTERFACE
                    , _NETWORK_INFO
                ) == false ) {
                    return false;
                }

                _loopEnd = true;
                return true;
            }
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:\"%s\"の検索に失敗\n"
                , _NETWORK_INFO.ssid.c_str()
            );
#endif  // DEBUG

            return false;
        }

        if( pspautoconnector2::connect(
            _NETWORK_INFO
            , _IW_SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "E:\"%s\"への接続に失敗\n"
                , _NETWORK_INFO.ssid.c_str()
            );
#endif  // DEBUG

            return false;
        }

        pspautoconnector2::changed( _networkStatusManager );

        return true;
    }

    void networkChangeProcNotConnected(
        pspautoconnector2::NetworkStatusManager &           _networkStatusManager
        , const int &                                       _IW_SOCKET
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , const std::string &                               _INTERFACE
        , pspautoconnector2::NetworkInfosManager &          _networkInfosManager
        , pspautoconnector2::NetworkInfos &                 _networkInfos
        , pspautoconnector2::NetworkInfoHistory &           _networkInfoHistory
        , const pspautoconnector2::Time &                   _RECONNECTABLE_USECONDS
        , const pspautoconnector2::Time &                   _TIMEOUT_FIND_NETWORK_USECONDS
        , const pspautoconnector2::Time &                   _INTERVAL_FIND_NETWORK_USECONDS
    )
    {
        const auto  NETWORK_INFO_PTR = getNextNetworkInfo(
            _networkInfos
            , _networkInfosManager
            , _networkInfoHistory
            , _RECONNECTABLE_USECONDS
        );
        if( NETWORK_INFO_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:次に接続するネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            pspautoconnector2::waitUpdate( _networkInfosManager );

            return;
        }
        const auto &    NETWORK_INFO = *NETWORK_INFO_PTR;

        if( networkChange(
            _networkStatusManager
            , _IW_SOCKET
            , _WE_VERSION
            , _INTERFACE
            , _networkInfoHistory
            , NETWORK_INFO
            , _TIMEOUT_FIND_NETWORK_USECONDS
            , _INTERVAL_FIND_NETWORK_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワークの変更に失敗\n" );
#endif  // DEBUG

            return;
        }
    }

    void networkChangeProcChanged(
        pspautoconnector2::NetworkStatusManager &   _networkStatusManager
    )
    {
        pspautoconnector2::waitWhenChanged( _networkStatusManager );
    }

    void networkChangeProcChecking(
        pspautoconnector2::NetworkStatusManager &   _networkStatusManager
    )
    {
        pspautoconnector2::waitWhenChecking( _networkStatusManager );
    }

    bool isForceChangeTarget(
        const std::set< std::string > &             _FORCE_CHANGE_TARGETS
        , const pspautoconnector2::NetworkInfo &    _NETWORK_INFO
    )
    {
        if( _FORCE_CHANGE_TARGETS.find( _NETWORK_INFO.ssid ) == _FORCE_CHANGE_TARGETS.end() ) {
            return false;
        }

        return true;
    }

    const pspautoconnector2::NetworkInfo * getForceChangeNetworkInfo(
        pspautoconnector2::NetworkInfos &               _networkInfos
        , pspautoconnector2::NetworkInfosManager &      _networkInfosManager
        , const pspautoconnector2::NetworkInfoHistory & _NETWORK_INFO_HISTORY
        , const pspautoconnector2::NetworkInfo &        _NETWORK_INFO
        , const size_t &                                _SSID_PREFIX_SIZE
        , const pspautoconnector2::Time &               _RECONNECTABLE_USECONDS
    )
    {
        pspautoconnector2::swapWhenUpdated(
            _networkInfos
            , _networkInfosManager
        );
        if( _networkInfos.size() <= 0 ) {
#ifdef  DEBUG
            std::printf( "D:ネットワーク情報が存在しない\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  NETWORK_INFO_PTR = pspautoconnector2::getForceChangeNetworkInfo(
            _NETWORK_INFO_HISTORY
            , _networkInfos
            , _NETWORK_INFO
            , _SSID_PREFIX_SIZE
            , _RECONNECTABLE_USECONDS
        );
        if( NETWORK_INFO_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:強制変更するネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return NETWORK_INFO_PTR;
    }

    void forceChange(
        pspautoconnector2::NetworkStatusManager &           _networkStatusManager
        , const int &                                       _IW_SOCKET
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , const std::string &                               _INTERFACE
        , pspautoconnector2::NetworkInfosManager &          _networkInfosManager
        , pspautoconnector2::NetworkInfos &                 _networkInfos
        , pspautoconnector2::NetworkInfoHistory &           _networkInfoHistory
        , const pspautoconnector2::NetworkInfo &            _NETWORK_INFO
        , const size_t &                                    _SSID_PREFIX_SIZE
        , const pspautoconnector2::Time &                   _RECONNECTABLE_USECONDS
        , const pspautoconnector2::Time &                   _TIMEOUT_FIND_NETWORK_USECONDS
        , const pspautoconnector2::Time &                   _INTERVAL_FIND_NETWORK_USECONDS
    )
    {
        const auto  NETWORK_INFO_PTR = getForceChangeNetworkInfo(
            _networkInfos
            , _networkInfosManager
            , _networkInfoHistory
            , _NETWORK_INFO
            , _SSID_PREFIX_SIZE
            , _RECONNECTABLE_USECONDS
        );
        if( NETWORK_INFO_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:強制変更するネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            pspautoconnector2::waitUpdate( _networkInfosManager );

            return;
        }
        const auto &    NETWORK_INFO = *NETWORK_INFO_PTR;

        if( networkChange(
            _networkStatusManager
            , _IW_SOCKET
            , _WE_VERSION
            , _INTERFACE
            , _networkInfoHistory
            , NETWORK_INFO
            , _TIMEOUT_FIND_NETWORK_USECONDS
            , _INTERVAL_FIND_NETWORK_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワークの変更に失敗\n" );
#endif  // DEBUG

            return;
        }
    }

    void networkChangeProcConnected(
        pspautoconnector2::NetworkStatusManager &           _networkStatusManager
        , const int &                                       _IW_SOCKET
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , const std::string &                               _INTERFACE
        , pspautoconnector2::NetworkInfosManager &          _networkInfosManager
        , pspautoconnector2::NetworkInfos &                 _networkInfos
        , pspautoconnector2::NetworkInfoHistory &           _networkInfoHistory
        , const bool &                                      _ENABLE_FORCE_CHANGE
        , const size_t &                                    _SSID_PREFIX_SIZE
        , const std::set< std::string > &                   _FORCE_CHANGE_TARGETS
        , const pspautoconnector2::Time &                   _RECONNECTABLE_USECONDS
        , const pspautoconnector2::Time &                   _TIMEOUT_FIND_NETWORK_USECONDS
        , const pspautoconnector2::Time &                   _INTERVAL_FIND_NETWORK_USECONDS
    )
    {
        //TODO
        const auto &    NETWORK_INFO = _networkInfoHistory.history.rbegin()->networkInfo;

        if( _ENABLE_FORCE_CHANGE == true &&
            isForceChangeTarget(
                _FORCE_CHANGE_TARGETS
                , NETWORK_INFO
            ) == true
        ) {
            forceChange(
                _networkStatusManager
                , _IW_SOCKET
                , _WE_VERSION
                , _INTERFACE
                , _networkInfosManager
                , _networkInfos
                , _networkInfoHistory
                , NETWORK_INFO
                , _SSID_PREFIX_SIZE
                , _RECONNECTABLE_USECONDS
                , _TIMEOUT_FIND_NETWORK_USECONDS
                , _INTERVAL_FIND_NETWORK_USECONDS
            );
        } else {
            pspautoconnector2::waitNotConnected( _networkStatusManager );
        }
    }

    void threadProcNetworkChange(
        const pspautoconnector2::ConfigFile &       _CONFIG_FILE
        , pspautoconnector2::NetworkStatusManager & _networkStatusManager
        , pspautoconnector2::NetworkInfosManager &  _networkInfosManager
    )
    {
        const auto &    CHANGER = _CONFIG_FILE.changer;
        const auto &    INTERFACE = CHANGER.interface;
        const auto &    TIMEOUT_FIND_NETWORK_USECONDS = CHANGER.timeoutFindNetworkUseconds;
        const auto &    INTERVAL_FIND_NETWORK_USECONDS = CHANGER.intervalFindNetworkUseconds;
        const auto &    HISTORY_SIZE = CHANGER.historySize;
        const auto &    RECONNECTABLE_USECONDS = CHANGER.reconnectableUseconds;
        const auto &    ENABLE_FORCE_CHANGE = CHANGER.enableForceChange;
        const auto &    SSID_PREFIX_SIZE = CHANGER.ssidPrefixSize;
        const auto &    FORCE_CHANGE_TARGETS = CHANGER.forceChangeTargets;

        auto    iwSocket = int( 0 );
        auto    weVersion = decltype( iw_range::we_version_compiled )();
        if( initIwSocket(
            iwSocket
            , weVersion
            , INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索と設定用ソケット初期化に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto    iwSocketCloser = pspautoconnector2::IwSocketCloser( &iwSocket );

        auto    networkInfos = pspautoconnector2::NetworkInfos();
        auto    networkInfoHistory = pspautoconnector2::NetworkInfoHistory{
            HISTORY_SIZE
        };

        while( 1 ) {
            const auto  STATUS = pspautoconnector2::getNetworkStatus( _networkStatusManager );

            switch( STATUS ) {
            case pspautoconnector2::NetworkStatus::NOT_CONNECTED:
                networkChangeProcNotConnected(
                    _networkStatusManager
                    , iwSocket
                    , weVersion
                    , INTERFACE
                    , _networkInfosManager
                    , networkInfos
                    , networkInfoHistory
                    , RECONNECTABLE_USECONDS
                    , TIMEOUT_FIND_NETWORK_USECONDS
                    , INTERVAL_FIND_NETWORK_USECONDS
                );
                break;

            case pspautoconnector2::NetworkStatus::CHANGED:
                networkChangeProcChanged( _networkStatusManager );
                break;

            case pspautoconnector2::NetworkStatus::CHECKING:
                networkChangeProcChecking( _networkStatusManager );
                break;

            case pspautoconnector2::NetworkStatus::CONNECTED:
                networkChangeProcConnected(
                    _networkStatusManager
                    , iwSocket
                    , weVersion
                    , INTERFACE
                    , _networkInfosManager
                    , networkInfos
                    , networkInfoHistory
                    , ENABLE_FORCE_CHANGE
                    , SSID_PREFIX_SIZE
                    , FORCE_CHANGE_TARGETS
                    , RECONNECTABLE_USECONDS
                    , TIMEOUT_FIND_NETWORK_USECONDS
                    , INTERVAL_FIND_NETWORK_USECONDS
                );
                break;

            default:
#ifdef  DEBUG
                std::printf( "E:ネットワークステータスが不正\n" );
#endif  // DEBUG

                break;
            }
        }
    }

    bool initSocket(
        int &                   _socket
        , const std::string &   _INTERFACE
    )
    {
        auto    socket = int( 0 );

        if( pspautoconnector2::initSocket( socket ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク接続確認用ソケット生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto    socketCloser = pspautoconnector2::SocketCloser( &socket );

        if( pspautoconnector2::bind(
            socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ソケットのバインドに失敗\n" );
#endif  // DEBUG

            return false;
        }

        socketCloser.release();

        _socket = std::move( socket );

        return true;
    }

    void connect(
        const pspautoconnector2::ConfigFile &       _CONFIG_FILE
        , pspautoconnector2::NetworkStatusManager & _networkStatusManager
    )
    {
        const auto &    CONNECTOR = _CONFIG_FILE.connector;
        const auto &    INTERFACE = CONNECTOR.interface;
        const auto &    PSP_MAC_ADDRESS = CONNECTOR.pspMacAddress;
        const auto &    TIMEOUT_CHECK_CONNECT_USECONDS = CONNECTOR.timeoutCheckConnectUseconds;

        auto    socket = int( 0 );
        if( initSocket(
            socket
            , INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク接続確認用ソケットの初期化に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto    socketCloser = pspautoconnector2::SocketCloser( &socket );

        while( 1 ) {
            pspautoconnector2::waitWhenNotConnected( _networkStatusManager );

            pspautoconnector2::checkingWhenChanged( _networkStatusManager );

            if( pspautoconnector2::isExistsPsp(
                socket
                , PSP_MAC_ADDRESS
                , TIMEOUT_CHECK_CONNECT_USECONDS
            ) == false ) {
                if( pspautoconnector2::missingWhenNotChanged( _networkStatusManager ) == false ) {
                    continue;
                }

#ifdef  DEBUG
                std::printf( "E:PSPが存在しない\n" );
#endif  // DEBUG

                continue;
            }

            pspautoconnector2::connected( _networkStatusManager );
        }
    }
}

int main(
    int         _argc
    , char **   _argv
)
{
    auto    configFile = pspautoconnector2::ConfigFile();
    if( initConfigFile(
        configFile
        , _argc
        , _argv
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:設定ファイルデータの生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    pspautoconnector2::NetworkInfosManager  networkInfosManager;

    auto    threadGetNetworkInfos = std::thread();
    if( initThread(
        threadGetNetworkInfos
        , [
            &configFile
            , &networkInfosManager
        ]
        {
            threadProcUpdateNetworkInfos(
                configFile
                , networkInfosManager
            );
        }
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:ネットワーク情報取得スレッドの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    pspautoconnector2::NetworkStatusManager networkStatusManager;

    auto    threadNetworkChange = std::thread();
    if( initThread(
        threadNetworkChange
        , [
            &configFile
            , &networkInfosManager
            , &networkStatusManager
        ]
        {
            threadProcNetworkChange(
                configFile
                , networkStatusManager
                , networkInfosManager
            );
        }
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:ネットワーク変更スレッドの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    connect(
        configFile
        , networkStatusManager
    );

    threadGetNetworkInfos.join();
    threadNetworkChange.join();

    return 0;
}
