﻿#include "pspautoconnector2/config/element/string.h"

#include <map>
#include <string>
#include <new>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BEGIN_STRING_ELEMENT = '"';
    const auto  BEGIN_STRING_ELEMENT_LENGTH = 1;
    const auto  END_STRING_ELEMENT = '"';
    const auto  END_STRING_ELEMENT_LENGTH = 1;

    const auto  ESCAPE = '\\';

    const auto  ESCAPE_MAP = std::map< char, char >(
        {
            { '\\', '\\' },
            { '"', '"' },
        }
    );

    const char * getString(
        const char *    _ELEMENT
        , const char *  _END
    )
    {
        const auto  STRING = _ELEMENT + BEGIN_STRING_ELEMENT_LENGTH;

        if( STRING > _END ) {
#ifdef  DEBUG
            std::printf( "D:文字列要素の始端文字が出現する前に文字列の最後に達している\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( _ELEMENT[ 0 ] != BEGIN_STRING_ELEMENT ) {
#ifdef  DEBUG
            std::printf(
                "D:%cで始まっていないため文字列要素ではない\n"
                , BEGIN_STRING_ELEMENT
            );
#endif  // DEBUG

            return nullptr;
        }

        return STRING;
    }

    const char * getEndString(
        const char *    _STRING
        , const char *  _END
    )
    {
        auto    escaped = false;
        auto    endString = static_cast< const char * >( nullptr );

        const auto  LENGTH = size_t( _END - _STRING );
        for( auto i = size_t( 0 ) ; i < LENGTH ; i++ ) {
            if( escaped == false ) {
                const auto &    CHAR_REF = _STRING[ i ];

                if( CHAR_REF == END_STRING_ELEMENT ) {
                    endString = &CHAR_REF;

                    break;
                } else if( CHAR_REF == ESCAPE ) {
                    escaped = true;
                }
            } else {
                escaped = false;
            }
        }

        if( endString == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:文字列要素の終端文字が存在しない\n" );
#endif  // DEBUG

            return nullptr;
        }

        return endString;
    }

    void initEscapedString(
        std::string &   _string
        , char          _escaped
    )
    {
        _string.push_back( ESCAPE );
        _string.push_back( _escaped );
    }

    void unescape(
        std::string &   _string
        , char          _escaped
        , char          _unescaped
    )
    {
        auto    escapedString = std::string();
        initEscapedString(
            escapedString
            , _escaped
        );
        const auto  ESCAPED_STRING_LENGTH = escapedString.length();

        auto    position = size_t( 0 );
        while( 1 ) {
            position = _string.find(
                escapedString
                , position
            );
            if( position == std::string::npos ) {
                break;
            }

            _string.replace(
                position
                , ESCAPED_STRING_LENGTH
                , &_unescaped
                , 1
            );
        }
    }

    void unescape(
        std::string &   _string
    )
    {
        for( const auto & ESCAPE_PAIR : ESCAPE_MAP ) {
            const auto &    ESCAPED = ESCAPE_PAIR.first;
            const auto &    UNESCAPED = ESCAPE_PAIR.second;

            unescape(
                _string
                , ESCAPED
                , UNESCAPED
            );
        }
    }

    pspautoconnector2::ConfigElementString * newString(
        const char *    _STRING
        , size_t        _length
    )
    {
        auto    thisUnique = pspautoconnector2::ConfigElementStringUnique(
            new( std::nothrow )pspautoconnector2::ConfigElementString(
                _STRING
                , _length
            )
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:文字列生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }
        auto &  string = *thisUnique;

        unescape( string );

        return thisUnique.release();
    }

    const char * getEndElement(
        const char *    _END_STRING
    )
    {
        return _END_STRING + END_STRING_ELEMENT_LENGTH;
    }
}

namespace pspautoconnector2 {
    const char * init(
        ConfigElementStringUnique & _stringUnique
        , const char *              _ELEMENT
        , const char *              _END
    )
    {
        const auto  STRING = getString(
            _ELEMENT
            , _END
        );
        if( STRING == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:文字列開始位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  END_STRING = getEndString(
            STRING
            , _END
        );
        if( END_STRING == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:文字列終了位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  LENGTH = size_t( END_STRING - STRING );

        auto    stringUnique = ConfigElementStringUnique(
            newString(
                STRING
                , LENGTH
            )
        );
        if( stringUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:文字列生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        const auto  END_ELEMENT = getEndElement( END_STRING );
        if( END_ELEMENT == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:要素終了位置取得に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        _stringUnique = std::move( stringUnique );

        return END_ELEMENT;
    }
}
