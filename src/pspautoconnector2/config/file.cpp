﻿#include "pspautoconnector2/config/file.h"
#include "pspautoconnector2/config/element/element.h"
#include "pspautoconnector2/config/element/map.h"
#include "pspautoconnector2/config/element/list.h"
#include "pspautoconnector2/time.h"
#include "pspautoconnector2/socket.h"

#include <string>
#include <set>
#include <cstdlib>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  KEY_SEARCHER = "searcher";
    const auto  KEY_SEARCHER_INTERFACE = "interface";
    const auto  KEY_SEARCHER_INTERVAL_USECONDS = "intervalUseconds";

    const auto  KEY_CHANGER = "changer";
    const auto  KEY_CHANGER_INTERFACE = "interface";
    const auto  KEY_CHANGER_TIMEOUT_FIND_NETWORK_USECONDS = "timeoutFindNetworkUseconds";
    const auto  KEY_CHANGER_INTERVAL_FIND_NETWORK_USECONDS = "intervalFindNetworkUseconds";
    const auto  KEY_CHANGER_HISTORY_SIZE = "historySize";
    const auto  KEY_CHANGER_RECONNECTABLE_USECONDS = "reconnectableUseconds";
    const auto  KEY_CHANGER_ENABLE_FORCE_CHANGE = "enableForceChange";
    const auto  KEY_CHANGER_SSID_PREFIX_SIZE = "ssidPrefixSize";
    const auto  KEY_CHANGER_FORCE_CHANGE_TARGETS = "forceChangeTargets";

    const auto  KEY_CONNECTOR = "connector";
    const auto  KEY_CONNECTOR_INTERFACE = "interface";
    const auto  KEY_CONNECTOR_PSP_MAC_ADDRESS = "pspMacAddress";
    const auto  KEY_CONNECTOR_TIMEOUT_FIND_NETWORK_USECONDS = "timeoutFindNetworkUseconds"; //REMOVEME
    const auto  KEY_CONNECTOR_INTERVAL_FIND_NETWORK_USECONDS = "intervalFindNetworkUseconds";   //REMOVEME
    const auto  KEY_CONNECTOR_TIMEOUT_CONNECT_USECONDS = "timeoutConnectUseconds";  //REMOVEME
    const auto  KEY_CONNECTOR_TIMEOUT_CHECK_CONNECT_USECONDS = "timeoutCheckConnectUseconds";

    bool strToSize(
        size_t &                _size
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    size = std::strtoull(
            _STRING.c_str()
            , &endPtr
            , 10
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:サイズの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:サイズに数値以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _size = std::move( size );

        return true;
    }

    bool initSearcherInterface(
        std::string &                                   _interface
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_SEARCHER_INTERFACE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定に%s要素が存在しない\n"
                , KEY_SEARCHER_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  INTERFACE_PTR = pspautoconnector2::getString( CONFIG );
        if( INTERFACE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定の%s要素が文字列要素ではない\n"
                , KEY_SEARCHER_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    INTERFACE = *INTERFACE_PTR;

        _interface = INTERFACE;

        return true;
    }

    bool initSearcherIntervalUseconds(
        pspautoconnector2::Time &                       _interval
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_SEARCHER_INTERVAL_USECONDS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定に%s要素が存在しない\n"
                , KEY_SEARCHER_INTERVAL_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  INTERVAL_PTR = pspautoconnector2::getString( CONFIG );
        if( INTERVAL_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定の%s要素が文字列要素ではない\n"
                , KEY_SEARCHER_INTERVAL_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    INTERVAL = *INTERVAL_PTR;

        if( pspautoconnector2::strToTime(
            _interval
            , INTERVAL
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列の時間化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initSearcher(
        pspautoconnector2::ConfigFile::Searcher &       _searcher
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_SEARCHER );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイルに%s要素が存在しない\n"
                , KEY_SEARCHER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  MAP_PTR = pspautoconnector2::getMap( CONFIG );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%s要素がマップ要素ではない\n"
                , KEY_SEARCHER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = *MAP_PTR;

        if( initSearcherInterface(
            _searcher.interface
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索用インターフェース要素の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initSearcherIntervalUseconds(
            _searcher.intervalUseconds
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索インターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerInterface(
        std::string &                                   _interface
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_INTERFACE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定に%s要素が存在しない\n"
                , KEY_CHANGER_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  INTERFACE_PTR = pspautoconnector2::getString( CONFIG );
        if( INTERFACE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    INTERFACE = *INTERFACE_PTR;

        _interface = INTERFACE;

        return true;
    }

    bool initChangerTimeoutFindNetworkUseconds(
        pspautoconnector2::Time &                       _timeout
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_TIMEOUT_FIND_NETWORK_USECONDS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定に%s要素が存在しない\n"
                , KEY_CHANGER_TIMEOUT_FIND_NETWORK_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  TIMEOUT_PTR = pspautoconnector2::getString( CONFIG );
        if( TIMEOUT_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_TIMEOUT_FIND_NETWORK_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    TIMEOUT = *TIMEOUT_PTR;

        if( pspautoconnector2::strToTime(
            _timeout
            , TIMEOUT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列の時間化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerIntervalFindNetworkUseconds(
        pspautoconnector2::Time &                       _interval
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_INTERVAL_FIND_NETWORK_USECONDS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定に%s要素が存在しない\n"
                , KEY_CHANGER_INTERVAL_FIND_NETWORK_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  INTERVAL_PTR = pspautoconnector2::getString( CONFIG );
        if( INTERVAL_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_INTERVAL_FIND_NETWORK_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    INTERVAL = *INTERVAL_PTR;

        if( pspautoconnector2::strToTime(
            _interval
            , INTERVAL
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列の時間化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerHistorySize(
        size_t &                                        _size
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_HISTORY_SIZE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定に%s要素が存在しない\n"
                , KEY_CHANGER_HISTORY_SIZE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  SIZE_PTR = pspautoconnector2::getString( CONFIG );
        if( SIZE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_HISTORY_SIZE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    SIZE = *SIZE_PTR;

        if( strToSize(
            _size
            , SIZE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列のサイズ化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerReconnectableUseconds(
        pspautoconnector2::Time &                       _reconnectableUseconds
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_RECONNECTABLE_USECONDS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定に%s要素が存在しない\n"
                , KEY_CHANGER_RECONNECTABLE_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  RECONNECTABLE_USECONDS_PTR = pspautoconnector2::getString( CONFIG );
        if( RECONNECTABLE_USECONDS_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_RECONNECTABLE_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    RECONNECTABLE_USECONDS = *RECONNECTABLE_USECONDS_PTR;

        if( pspautoconnector2::strToTime(
            _reconnectableUseconds
            , RECONNECTABLE_USECONDS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列の時間化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerEnableForceChange(
        bool &                                          _enable
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_ENABLE_FORCE_CHANGE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定に%s要素が存在しない\n"
                , KEY_CHANGER_ENABLE_FORCE_CHANGE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  ENABLE_PTR = pspautoconnector2::getString( CONFIG );
        if( ENABLE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク検索設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_ENABLE_FORCE_CHANGE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    ENABLE = *ENABLE_PTR;

        _enable = ( ENABLE == "1" );

        return true;
    }

    bool initChangerSsidPrefixSize(
        size_t &                                        _size
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_SSID_PREFIX_SIZE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定に%s要素が存在しない\n"
                , KEY_CHANGER_SSID_PREFIX_SIZE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  SIZE_PTR = pspautoconnector2::getString( CONFIG );
        if( SIZE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_SSID_PREFIX_SIZE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    SIZE = *SIZE_PTR;

        if( strToSize(
            _size
            , SIZE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列のサイズ化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initChangerForceChangeTargets(
        std::set< std::string > &                       _forceChangeTargets
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER_FORCE_CHANGE_TARGETS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定に%s要素が存在しない\n"
                , KEY_CHANGER_FORCE_CHANGE_TARGETS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  FORCE_CHANGE_TARGETS_PTR = pspautoconnector2::getList( CONFIG );
        if( FORCE_CHANGE_TARGETS_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク変更設定の%s要素が文字列要素ではない\n"
                , KEY_CHANGER_FORCE_CHANGE_TARGETS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    FORCE_CHANGE_TARGETS = *FORCE_CHANGE_TARGETS_PTR;

        for( const auto & FORCE_CHANGE_TARGET_CONFIG_UNIQUE : FORCE_CHANGE_TARGETS ) {
            const auto &    FORCE_CHANGE_TARGET_CONFIG = *FORCE_CHANGE_TARGET_CONFIG_UNIQUE;

            const auto  FORCE_CHANGE_TARGET_PTR = pspautoconnector2::getString( FORCE_CHANGE_TARGET_CONFIG );
            if( FORCE_CHANGE_TARGET_PTR == nullptr ) {
#ifdef  DEBUG
                std::printf( "E:SSID強制変更対象リストの要素が文字列ではない\n" );
#endif  // DEBUG

                return false;
            }
            const auto &    FORCE_CHANGE_TARGET = *FORCE_CHANGE_TARGET_PTR;

            _forceChangeTargets.insert( FORCE_CHANGE_TARGET );
        }

        return true;
    }

    bool initChanger(
        pspautoconnector2::ConfigFile::Changer &        _changer
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CHANGER );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイルに%s要素が存在しない\n"
                , KEY_CHANGER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  MAP_PTR = pspautoconnector2::getMap( CONFIG );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%s要素がマップ要素ではない\n"
                , KEY_CHANGER
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = *MAP_PTR;

        if( initChangerInterface(
            _changer.interface
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク変更用インターフェース要素の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerTimeoutFindNetworkUseconds(
            _changer.timeoutFindNetworkUseconds
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク変更前検索のタイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerIntervalFindNetworkUseconds(
            _changer.intervalFindNetworkUseconds
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク変更前検索のインターバルの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerHistorySize(
            _changer.historySize
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク情報履歴サイズの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerReconnectableUseconds(
            _changer.reconnectableUseconds
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:再接続可能時間の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerEnableForceChange(
            _changer.enableForceChange
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID強制変更有効化の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerSsidPrefixSize(
            _changer.ssidPrefixSize
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの接頭辞のサイズ初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChangerForceChangeTargets(
            _changer.forceChangeTargets
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSID強制変更対象リストの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initConnectorInterface(
        std::string &                                   _interface
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CONNECTOR_INTERFACE );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定に%s要素が存在しない\n"
                , KEY_CONNECTOR_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  INTERFACE_PTR = pspautoconnector2::getString( CONFIG );
        if( INTERFACE_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定の%s要素が文字列要素ではない\n"
                , KEY_CONNECTOR_INTERFACE
            );
#endif  // DEBUG

            return false;
        }
        const auto &    INTERFACE = *INTERFACE_PTR;

        _interface = INTERFACE;

        return true;
    }

    bool initConnectorPspMacAddress(
        pspautoconnector2::MacAddress &                 _macAddress
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CONNECTOR_PSP_MAC_ADDRESS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定に%s要素が存在しない\n"
                , KEY_CONNECTOR_PSP_MAC_ADDRESS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  PSP_MAC_ADDRESS_PTR = pspautoconnector2::getString( CONFIG );
        if( PSP_MAC_ADDRESS_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定の%s要素が文字列要素ではない\n"
                , KEY_CONNECTOR_PSP_MAC_ADDRESS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    PSP_MAC_ADDRESS = *PSP_MAC_ADDRESS_PTR;

        if( pspautoconnector2::initMacAddress(
            _macAddress
            , PSP_MAC_ADDRESS
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initConnectorTimeoutCheckConnectUseconds(
        pspautoconnector2::Time &                       _timeout
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CONNECTOR_TIMEOUT_CHECK_CONNECT_USECONDS );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定に%s要素が存在しない\n"
                , KEY_CONNECTOR_TIMEOUT_CHECK_CONNECT_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  TIMEOUT_PTR = pspautoconnector2::getString( CONFIG );
        if( TIMEOUT_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:ネットワーク接続設定の%s要素が文字列要素ではない\n"
                , KEY_CONNECTOR_TIMEOUT_CHECK_CONNECT_USECONDS
            );
#endif  // DEBUG

            return false;
        }
        const auto &    TIMEOUT = *TIMEOUT_PTR;

        if( pspautoconnector2::strToTime(
            _timeout
            , TIMEOUT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:文字列の時間化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initConnector(
        pspautoconnector2::ConfigFile::Connector &      _connector
        , const pspautoconnector2::ConfigElementMap &   _MAP
    )
    {
        const auto  IT = _MAP.find( KEY_CONNECTOR );
        if( IT == _MAP.end() ) {
#ifdef  DEBUG
            std::printf(
                "E:設定ファイルに%s要素が存在しない\n"
                , KEY_CONNECTOR
            );
#endif  // DEBUG

            return false;
        }
        const auto &    CONFIG = *( IT->second );

        const auto  MAP_PTR = pspautoconnector2::getMap( CONFIG );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:%s要素がマップ要素ではない\n"
                , KEY_CONNECTOR
            );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = *MAP_PTR;

        if( initConnectorInterface(
            _connector.interface
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク接続用インターフェース要素の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initConnectorPspMacAddress(
            _connector.pspMacAddress
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:PSPのMACアドレスの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initConnectorTimeoutCheckConnectUseconds(
            _connector.timeoutCheckConnectUseconds
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク接続確認のタイムアウトの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

namespace pspautoconnector2 {
    bool initConfigFile(
        ConfigFile &            _configFile
        , const ConfigElement & _CONFIG
    )
    {
        const auto  MAP_PTR = getMap( _CONFIG );
        if( MAP_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:設定ファイルがマップ要素ではない\n" );
#endif  // DEBUG

            return false;
        }
        const auto &    MAP = *MAP_PTR;

        if( initSearcher(
            _configFile.searcher
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索設定の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initChanger(
            _configFile.changer
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク変更設定の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initConnector(
            _configFile.connector
            , MAP
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク接続設定の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}
