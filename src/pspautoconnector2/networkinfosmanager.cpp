﻿#include "pspautoconnector2/networkinfosmanager.h"
#include "pspautoconnector2/networkinfos.h"

#include <mutex>
#include <utility>

namespace pspautoconnector2 {
    bool update(
        NetworkInfosManager &                               _this
        , const int &                                       _IW_SOCKET
        , const std::string &                               _INTERFACE
        , const decltype( iw_range::we_version_compiled ) & _WE_VERSION
        , NetworkInfos &                                    _buffer
    )
    {
        if( getNetworkInfos(
            _buffer
            , _IW_SOCKET
            , _INTERFACE
            , _WE_VERSION
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク情報の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        std::swap(
            _this.networkInfos
            , _buffer
        );

        _this.updated = true;

        _this.condition.notify_all();

        return true;
    }

    void swapWhenUpdated(
        NetworkInfos &          _networkInfos
        , NetworkInfosManager & _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( _this.updated == false ) {
            return;
        }

        std::swap(
            _networkInfos
            , _this.networkInfos
        );

        _this.updated = false;
    }

    void waitUpdate(
        NetworkInfosManager &   _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        while( _this.updated == false ) {
            _this.condition.wait( lock );
        }
    }
}
