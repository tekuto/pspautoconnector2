﻿#include "pspautoconnector2/time.h"

#include <sys/time.h>
#include <unistd.h>
#include <cstdlib>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  SECOND_USECONDS = pspautoconnector2::Time( 1000000 );

    bool getCurrentTime(
        pspautoconnector2::Time &   _currentTime
    )
    {
        auto    timeVal = timeval();
        if( gettimeofday(
            &timeVal
            , nullptr
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:現在時刻の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _currentTime = timeVal.tv_sec * SECOND_USECONDS + timeVal.tv_usec;

        return true;
    }
}

namespace pspautoconnector2 {
    bool strToTime(
        Time &                  _time
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    time = std::strtoll(
            _STRING.c_str()
            , &endPtr
            , 10
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:時間の数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:時間に数値以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _time = std::move( time );

        return true;
    }

    bool intervalProc(
        const Time &                                _INTERVAL
        , const std::function< bool( bool & ) > &   _PROC
    )
    {
        while( true ) {
            auto    loopEnd = false;

            const auto  RESULT = _PROC( loopEnd );

            if( loopEnd == true ) {
                return RESULT;
            }

            usleep( _INTERVAL );
        }
    }

    bool timeoutProc(
        const Time &                                _TIMEOUT
        , const Time &                              _INTERVAL
        , const std::function< bool( bool & ) > &   _PROC
    )
    {
        auto    startTime = Time();
        if( getCurrentTime( startTime ) == false ) {
#ifdef  DEBUG
            std::printf( "E:開始時刻の取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return intervalProc(
            _INTERVAL
            , [
                &_TIMEOUT
                , &_PROC
                , &startTime
            ]
            (
                bool &  _loopEnd
            )
            {
                const auto  RESULT = _PROC( _loopEnd );

                if( _loopEnd == true ) {
                    return RESULT;
                }

                auto    currentTime = Time();
                if( getCurrentTime( currentTime ) == false ) {
#ifdef  DEBUG
                    std::printf( "E:現在時刻の取得に失敗\n" );
#endif  // DEBUG

                    _loopEnd = true;
                    return false;
                }
                if( currentTime - startTime > _TIMEOUT ) {
#ifdef  DEBUG
                    std::printf( "E:タイムアウト\n" );
#endif  // DEBUG

                    _loopEnd = true;
                    return false;
                }

                return true;
            }
        );
    }
}
