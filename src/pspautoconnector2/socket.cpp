﻿#include "pspautoconnector2/socket.h"
#include "pspautoconnector2/time.h"

#include <sys/socket.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <algorithm>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BYTE_STRING_LENGTH = 2;

    const auto  MAC_ADDRESS_ELEMENT_BITS = 8;

    const auto  FIND_INTERVAL_USECONDS = pspautoconnector2::Time( 500000 );

    typedef unsigned short ReadSize;

    typedef std::array<
        char
        , static_cast< ReadSize >( ~0 )
    > Buffer;

    bool getInterfaceIndex(
        decltype( sockaddr_ll::sll_ifindex ) &  _interfaceIndex
        , const int &                           _SOCKET
        , const std::string &                   _INTERFACE
    )
    {
        auto    ifReq = ifreq();
        std::memset(
            &ifReq
            , 0
            , sizeof( ifReq )
        );
        std::strncpy(
            ifReq.ifr_name
            , _INTERFACE.c_str()
            , sizeof( ifReq.ifr_name )
        );

        if( ioctl(
            _SOCKET
            , SIOCGIFINDEX
            , &ifReq
        ) < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ioctl()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _interfaceIndex = ifReq.ifr_ifindex;

        return true;
    }

    bool poll(
        const int & _SOCKET
    )
    {
        auto    pollFd = pollfd();
        std::memset(
            &pollFd
            , 0
            , sizeof( pollFd )
        );
        pollFd.fd = _SOCKET;
        pollFd.events = POLLIN;

        const auto  RESULT = ::poll(
            &pollFd
            , 1
            , 0
        );
        if( RESULT < 0 ) {
#ifdef  DEBUG
            std::printf( "E:poll()でエラー\n" );
#endif  // DEBUG

            return false;
        }
        if( RESULT == 0 ) {
            return false;
        }

        return true;
    }

    bool readData(
        Buffer &        _buffer
        , ReadSize &    _readSize
        , const int &   _SOCKET
    )
    {
        const auto  READ_SIZE = read(
            _SOCKET
            , _buffer.data()
            , _buffer.size()
        );
        if( READ_SIZE < -1 ) {
            return false;
        }

        _readSize = READ_SIZE;

        return true;
    }

    bool setMacAddress(
        pspautoconnector2::MacAddress & _macAddress
        , const Buffer &                _BUFFER
        , const ReadSize &              _READ_SIZE
    )
    {
        if( _READ_SIZE < pspautoconnector2::MAC_ADDRESS_LENGTH + _macAddress.size() ) {
#ifdef  DEBUG
            std::printf( "D:パケットサイズが小さすぎる\n" );
#endif  // DEBUG

            return false;
        }

        std::memcpy(
            _macAddress.data()
            , _BUFFER.data() + pspautoconnector2::MAC_ADDRESS_LENGTH
            , _macAddress.size()
        );

        return true;
    }

    bool readMacAddress(
        pspautoconnector2::MacAddress & _macAddress
        , const int &                   _SOCKET
    )
    {
        auto    buffer = Buffer();
        auto    readSize = ReadSize();
        if( readData(
            buffer
            , readSize
            , _SOCKET
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:データ読み込みに失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = pspautoconnector2::MacAddress();
        if( setMacAddress(
            macAddress
            , buffer
            , readSize
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:MACアドレスの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _macAddress = std::move( macAddress );

        return true;
    }

    //TODO 読めるデータは全て読むべき？
    bool checkMacAddress(
        const int &                             _SOCKET
        , const pspautoconnector2::MacAddress & _MAC_ADDRESS
    )
    {
        while( true ) {
            if( poll( _SOCKET ) == false ) {
                return false;
            }

            auto    macAddress = pspautoconnector2::MacAddress();
            if( readMacAddress(
                macAddress
                , _SOCKET
            ) == false ) {
                continue;
            }

            if( macAddress == _MAC_ADDRESS ) {
                break;
            }
        }

        return true;
    }

    bool strToLl(
        long long &             _ll
        , const std::string &   _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    ll = std::strtoll(
            _STRING.c_str()
            , &endPtr
            , 16
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスに16進数以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _ll = std::move( ll );

        return true;
    }

    void llToMacAddress(
        pspautoconnector2::MacAddress & _macAddress
        , long long                     _ll
    )
    {
        std::for_each(
            _macAddress.rbegin()
            , _macAddress.rend()
            , [
                &_ll
            ]
            (
                pspautoconnector2::MacAddress::value_type & _element
            )
            {
                _element = _ll;
                _ll >>= MAC_ADDRESS_ELEMENT_BITS;
            }
        );
    }
}

namespace pspautoconnector2 {
    void CloseSocket::operator()(
        int *   _socketPtr
    ) const
    {
        close( *_socketPtr );
    }

    bool initSocket(
        int &   _socket
    )
    {
        auto    socket_ = socket(
            AF_PACKET
            , SOCK_RAW
            , htons( ETH_P_ALL )
        );
        if( socket_ < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ソケットの生成に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _socket = std::move( socket_ );

        return true;
    }

    bool bind(
        const int &             _SOCKET
        , const std::string &   _INTERFACE
    )
    {
        auto    interfaceIndex = decltype( sockaddr_ll::sll_ifindex )();
        if( getInterfaceIndex(
            interfaceIndex
            , _SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワークインターフェースのインデックス取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    addr = sockaddr_ll();
        std::memset(
            &addr
            , 0
            , sizeof( addr )
        );
        addr.sll_family = AF_PACKET;
        addr.sll_protocol = htons( ETH_P_ALL );
        addr.sll_ifindex = interfaceIndex;

        if( bind(
            _SOCKET
            , reinterpret_cast< const sockaddr * >( &addr )
            , sizeof( addr )
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:ソケットのバインドに失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool isExistsPsp(
        const int &             _SOCKET
        , const MacAddress &    _MAC_ADDRESS
        , const Time &          _TIMEOUT
    )
    {
        if( timeoutProc(
            _TIMEOUT
            , FIND_INTERVAL_USECONDS
            , [
                &_SOCKET
                , &_MAC_ADDRESS
            ]
            (
                bool &  _loopEnd
            )
            {
                if( checkMacAddress(
                    _SOCKET
                    , _MAC_ADDRESS
                ) == true ) {

                    _loopEnd = true;

                    return true;
                }

                return false;
            }
        ) == false ) {
            return false;
        }

        return true;
    }

    bool initMacAddress(
        MacAddress &            _macAddress
        , const std::string &   _STRING
    )
    {
        if( _STRING.size() != MAC_ADDRESS_LENGTH * BYTE_STRING_LENGTH ) {
#ifdef  DEBUG
            std::printf( "E:文字列の長さがMACアドレスの長さではない\n" );
#endif  // DEBUG

            return false;
        }

        long long   macAddressLl;
        if( strToLl(
            macAddressLl
            , _STRING
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = MacAddress();
        llToMacAddress(
            macAddress
            , macAddressLl
        );

        _macAddress = std::move( macAddress );

        return true;
    }
}
